import React from "react";
import PropTypes from "prop-types";

import IconWrapper from "../icon-wrapper";

export const LocationIcon = ({ size, color }) => {
  return (
    <IconWrapper size={size}>
      <svg
        className="bi bi-geo-alt"
        width="1em"
        height="1em"
        viewBox="0 0 16 16"
        fill={color}
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          fillRule="evenodd"
          d="M8 16s6-5.686 6-10A6 6 0 002 6c0 4.314 6 10 6 10zm0-7a3 3 0 100-6 3 3 0 000 6z"
          clipRule="evenodd"
        />
      </svg>
    </IconWrapper>
  );
};

LocationIcon.propTypes = {
  size: PropTypes.number,
  color: PropTypes.string
};

LocationIcon.defaultProps = {
  size: null,
  color: "currentColor"
};
