import { createEffect, createStore } from "effector";

/**
 * Facebook App Init
 */
export const fbInit = createEffect("Facebook App Init").use(
  async () =>
    await new Promise(resolve => {
      window.fbAsyncInit = () => {
        window.FB.init({
          appId: "537190380280407",
          cookie: true,
          xfbml: true,
          version: "v6.0"
        });
        window.FB.AppEvents.logPageView();
        getLogStatus();

        return resolve(true);
      };
    })
);

/**
 * Log In
 */
export const login = createEffect("Log In").use(
  () =>
    new Promise(resolve => window.FB.login(response => resolve(response)), {
      scope:
        "public_profile, user_photos, publish_pages, publish_pages, manage_pages"
    })
);

/**
 * Log Out
 */
export const logout = createEffect("Log Out").use(
  () => new Promise(resolve => window.FB.logout(response => resolve(response)))
);

/**
 * Check Log Status
 */
export const getLogStatus = createEffect("Check Log Status").use(
  () =>
    new Promise(resolve =>
      window.FB.getLoginStatus(response => resolve(response))
    )
);

/**
 * Log (Auth) Status
 */
export const $logStatus = createStore(null).on(
  [login.done, logout.done, getLogStatus.done],
  (_, { result }) => result.status
);

export const $accessToken = createStore(null).on(
  getLogStatus.done,
  (_, { result }) => {
    if (result.status === "connected") {
      localStorage.setItem("accessToken", result.authResponse.accessToken);
      localStorage.setItem("userID", result.authResponse.userID);

      return {
        accessToken: result.authResponse.accessToken,
        userID: result.authResponse.userID
      };
    } else {
      localStorage.removeItem("accessToken");
      localStorage.removeItem("userID");
    }
  }
);

/**
 * Get User Info
 */
export const getUserInfo = createEffect("Get User Info").use(
  () =>
    new Promise(resolve =>
      window.FB.api(
        "/me",
        { fields: ["picture", "first_name", "last_name"] },
        response => resolve(response)
      )
    )
);

/**
 * Fetch User Info If Connected
 */
$logStatus.watch(status => {
  if (status === "connected") {
    getUserInfo();
  }
});

/**
 * User Info
 */
export const $userInfo = createStore(null)
  .on(getUserInfo.done, (_, { result }) => result)
  .reset(logout.done);
