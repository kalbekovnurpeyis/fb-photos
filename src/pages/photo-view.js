import React from "react";
import { useStore } from "effector-react";
import { Col, Row, Progress } from "reactstrap";

import { AlbumHeader } from "../components/album-header";
import { getSinglePhoto, $singlePhoto } from "../models/fb-albums";
import { CustomTable } from "../components/custom-table";

export const PhotoView = ({ match }) => {
  const singlePhoto = useStore($singlePhoto);
  const getSinglePhotoPending = useStore(getSinglePhoto.pending);

  React.useEffect(() => {
    if (match.params.photoId) {
      getSinglePhoto(match.params.photoId);
    }
  }, [match.params.photoId]);

  return getSinglePhotoPending ? (
    <Progress animated value={100} />
  ) : (
    <>
      <AlbumHeader title="Photo" />

      {singlePhoto && (
        <Row>
          <Col lg={6}>
            <img
              src={singlePhoto.images[0].source}
              alt="test"
              style={{ maxWidth: "100%" }}
            />
          </Col>
          <Col lg={6}>
            <CustomTable data={{ Caption: singlePhoto.name }} />
            {/* <table>
              <tbody>
                {singlePhoto.name && (
                  <tr>
                    <td>Caption: </td>
                    <td>
                      <div style={{ maxWidth: 500 }}>{singlePhoto.name}</div>
                    </td>
                  </tr>
                )}
              </tbody>
            </table> */}
          </Col>
        </Row>
      )}
    </>
  );
};
