import React from "react";
import { useStore } from "effector-react";
import { Progress } from "reactstrap";
import "rc-tooltip/assets/bootstrap.css";

import { AlbumHeader } from "../components/album-header";
import { PhotoPreview } from "../components/photo-preview";
import { getAlbum, $singleAlbum } from "../models/fb-albums";

export const AlbumsView = ({ match }) => {
  const singleAlbum = useStore($singleAlbum);
  const getAlbumPending = useStore(getAlbum.pending);

  React.useEffect(() => {
    if (match && match.params) {
      getAlbum(match.params.albumId);
    }
  }, [match]);

  return getAlbumPending ? (
    <Progress animated value={100} />
  ) : (
    <>
      {singleAlbum && (
        <>
          <AlbumHeader title={singleAlbum.name} />

          <div style={{ display: "flex", flexWrap: "wrap" }}>
            {singleAlbum.photos
              ? singleAlbum.photos.data.map(photo => (
                  <PhotoPreview
                    key={photo.id}
                    photo={photo}
                    albumId={match.params.albumId}
                  />
                ))
              : "No Photo Found"}
          </div>
        </>
      )}
    </>
  );
};
