import React from "react";
import { useStore } from "effector-react";
import { Progress } from "reactstrap";

import { Album } from "../components/album";
import { getUserAlbums, $userAlbums } from "../models/fb-albums";

export const AlbumsList = () => {
  const userAlbums = useStore($userAlbums);
  const getUserAlbumsPending = useStore(getUserAlbums.pending);

  React.useEffect(() => {
    getUserAlbums();
  }, []);

  return getUserAlbumsPending ? (
    <Progress animated value={100} />
  ) : (
    <>
      {userAlbums && userAlbums.data
        ? userAlbums.data.map(album => <Album key={album.id} album={album} />)
        : "No album found"}
    </>
  );
};
