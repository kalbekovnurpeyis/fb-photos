import React from "react";
import styled from "styled-components";
import { NavLink } from "react-router-dom";
import Tooltip from "rc-tooltip";
import "rc-tooltip/assets/bootstrap.css";

import { CheckIcon } from "../components/icons";
import { CustomTable } from "../components/custom-table";

export const PhotoPreview = ({ photo, albumId }) => {
  return (
    <>
      <Tooltip
        placement="bottomLeft"
        overlay={() => <CustomTable data={{ Caption: photo.alt_text }} />}
      >
        <Wrapper
          to={`/albums/${albumId}/${photo.id}`}
          data-place="bottom"
          data-tip
          data-for={`tooltip-${photo.id}`}
        >
          <PreviewImage bgImage={getImage(photo.images)} />
          <div style={{ display: "flex", alignItems: "center", padding: 10 }}>
            <CheckIcon />
            <span className="ml-2">Select</span>
          </div>
        </Wrapper>
      </Tooltip>
    </>
  );
};

const getImage = images => {
  if (images.length) {
    return images[images.length - 1].source;
  }

  return "https://www.caa.com/sites/default/files/styles/headshot_500x500/public/2019-07/Kunis_CAASpeakers_Headshot.jpg?itok=ho3100AP";
};

const Wrapper = styled(NavLink)`
  width: calc((100% - 60px) / 6);
  border: 1px solid #eee;
  border-radius: 5px;
  margin: 5px;
  overflow: hidden;
`;

const PreviewImage = styled.div`
  max-width: 100%;
  height: 150px;
  background-image: url(${({ bgImage }) => bgImage});
  background-size: cover;
  background-position: center;

  img {
    max-width: 100%;
  }
`;
