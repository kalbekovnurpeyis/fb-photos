import React from "react";
import PropTypes from "prop-types";

import IconWrapper from "../icon-wrapper";

export const ArrowDoubleRightIcon = ({ size, color }) => {
  return (
    <IconWrapper size={size}>
      <svg
        className="bi bi-chevron-double-right"
        width="1em"
        height="1em"
        viewBox="0 0 16 16"
        fill={color}
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          fillRule="evenodd"
          d="M3.646 1.646a.5.5 0 01.708 0l6 6a.5.5 0 010 .708l-6 6a.5.5 0 01-.708-.708L9.293 8 3.646 2.354a.5.5 0 010-.708z"
          clipRule="evenodd"
        />
        <path
          fillRule="evenodd"
          d="M7.646 1.646a.5.5 0 01.708 0l6 6a.5.5 0 010 .708l-6 6a.5.5 0 01-.708-.708L13.293 8 7.646 2.354a.5.5 0 010-.708z"
          clipRule="evenodd"
        />
      </svg>
    </IconWrapper>
  );
};

ArrowDoubleRightIcon.propTypes = {
  size: PropTypes.number,
  color: PropTypes.string
};

ArrowDoubleRightIcon.defaultProps = {
  size: null,
  color: "currentColor"
};
