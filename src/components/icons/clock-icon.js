import React from "react";
import PropTypes from "prop-types";

import IconWrapper from "../icon-wrapper";

export const ClockIcon = ({ size, color }) => {
  return (
    <IconWrapper size={size}>
      <svg
        className="bi bi-clock"
        width="1em"
        height="1em"
        viewBox="0 0 16 16"
        fill={color}
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          fillRule="evenodd"
          d="M8 15A7 7 0 108 1a7 7 0 000 14zm8-7A8 8 0 110 8a8 8 0 0116 0z"
          clipRule="evenodd"
        />
        <path
          fillRule="evenodd"
          d="M7.5 3a.5.5 0 01.5.5v5.21l3.248 1.856a.5.5 0 01-.496.868l-3.5-2A.5.5 0 017 9V3.5a.5.5 0 01.5-.5z"
          clipRule="evenodd"
        />
      </svg>
    </IconWrapper>
  );
};

ClockIcon.propTypes = {
  size: PropTypes.number,
  color: PropTypes.string
};

ClockIcon.defaultProps = {
  size: null,
  color: "currentColor"
};
