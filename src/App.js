import React from "react";
import { renderRoutes } from "react-router-config";
import { Container, Progress } from "reactstrap";
import { useStore } from "effector-react";

import { Header } from "./components/header";
import { routes } from "./routes";
import { fbInit, getLogStatus } from "./models/fb-auth";

function App() {
  const fbPending = useStore(fbInit.pending);
  const logPending = useStore(getLogStatus.pending);

  return (
    <>
      <Header />
      <Container>
        {fbPending || logPending ? (
          <Progress animated value={100} />
        ) : (
          renderRoutes(routes)
        )}
      </Container>
    </>
  );
}

export default App;
