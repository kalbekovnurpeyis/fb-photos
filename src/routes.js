import React from "react";
import {
  Albums,
  AlbumsList,
  AlbumsView,
  Auth,
  NotFound,
  PhotoView,
  UploadPhotos
} from "./pages";
import { Redirect } from "react-router-dom";

import {
  ChatIcon,
  ImageIcon,
  LocationIcon,
  UploadIcon
} from "./components/icons";

export const routes = [
  {
    path: "/login",
    exact: true,
    component: Auth
  },
  {
    path: "/",
    exact: true,
    component: () => <Redirect to="/albums" />
  },
  {
    path: "/albums",
    component: Albums,
    icon: ImageIcon,
    isTab: true,
    routes: [
      {
        path: "/albums",
        exact: true,
        component: AlbumsList
      },
      {
        path: "/albums/:albumId",
        exact: true,
        component: AlbumsView
      },
      {
        path: "/albums/:albumId/:photoId",
        exact: true,
        component: PhotoView
      },
      {
        path: "/albums",
        component: () => <Redirect to="/404" />
      }
    ]
  },
  {
    path: "/location",
    exact: true,
    component: () => "<h1>Disabled Location Page<h1>",
    icon: LocationIcon,
    isTab: true,
    isDisabled: true
  },
  {
    path: "/comments",
    exact: true,
    component: () => "<h1>Disabled Comments Page<h1>",
    icon: ChatIcon,
    isTab: true,
    isDisabled: true
  },
  {
    path: "/upload",
    exact: true,
    component: UploadPhotos,
    icon: UploadIcon,
    isTab: true
  },
  {
    path: "*",
    component: NotFound
  }
];
