import React from "react";
import PropTypes from "prop-types";

const IconWrapper = ({ children, size }) => {
  return <div style={{ fontSize: `${size || 16}px` }}>{children}</div>;
};

export default IconWrapper;

IconWrapper.propTypes = {
  children: PropTypes.node,
  size: PropTypes.number
};

IconWrapper.defaultProps = {
  size: null
};
