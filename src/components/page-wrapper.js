import React from "react";
import { useHistory } from "react-router-dom";
import { useStore } from "effector-react";

import { NavTab } from "./nav-tab";
import { $logStatus } from "../models/fb-auth";

export const PageWrapper = ({ children }) => {
  const logStatus = useStore($logStatus);
  const history = useHistory();

  React.useEffect(() => {
    if (logStatus !== "connected") {
      history.replace("/login");
    }
  });

  return (
    <>
      <NavTab />
      <div className="mt-3">{children}</div>
    </>
  );
};
