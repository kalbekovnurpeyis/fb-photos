import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

export const CustomTable = ({ data }) => {
  return data ? (
    <Wrapper>
      <table>
        <tbody>
          {Object.keys(data).map(
            key =>
              data[key] && (
                <tr key={key}>
                  <td>{key}: </td>
                  <td>
                    <div style={{ maxWidth: 500 }}>{data[key]}</div>
                  </td>
                </tr>
              )
          )}
        </tbody>
      </table>
    </Wrapper>
  ) : (
    ""
  );
};

CustomTable.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string
  }).isRequired
};

const Wrapper = styled.div`
  td:first-child {
    padding-right: 10px;
  }
`;
