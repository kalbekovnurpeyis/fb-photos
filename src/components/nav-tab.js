import React from "react";
import { NavLink as Link } from "react-router-dom";
import { Nav, NavItem, NavLink } from "reactstrap";

import { routes } from "../routes";

export const NavTab = () => {
  return (
    <Nav tabs>
      {routes
        .filter(({ isTab }) => isTab)
        .map(({ path, icon, isDisabled }) => (
          <NavItem key={path}>
            <NavLink
              tag={Link}
              to={path}
              activeClassName="active"
              disabled={isDisabled}
            >
              {React.createElement(icon, {
                size: 24,
                color: isDisabled ? "#6c757d" : "#3b60b8"
              })}
            </NavLink>
          </NavItem>
        ))}
    </Nav>
  );
};
