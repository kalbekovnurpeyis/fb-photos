import React from "react";
import { renderRoutes } from "react-router-config";

import { PageWrapper } from "../components/page-wrapper";

export const Albums = ({ route }) => {
  return <PageWrapper>{renderRoutes(route.routes)}</PageWrapper>;
};
