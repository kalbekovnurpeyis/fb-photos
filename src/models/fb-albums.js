import { createEffect, createStore } from "effector";

/**
 * Get User Album List
 */
export const getUserAlbums = createEffect("Get User Album List").use(
  async () =>
    await new Promise(resolve =>
      window.FB.api(
        "/me/albums",
        {
          fields: "id, name, created_time, cover_photo{picture}, count"
        },
        response => resolve(response)
      )
    )
);

/**
 * User Album List
 */
export const $userAlbums = createStore(null).on(
  getUserAlbums.done,
  (_, { result }) => result
);

/**
 * Get Album
 */
export const getAlbum = createEffect("Get Album").use(
  async id =>
    await new Promise(resolve =>
      window.FB.api(
        `/${id}`,
        {
          fields: "id, name, photos{id, name, images, alt_text}"
        },
        response => resolve(response)
      )
    )
);

/**
 * Single Album
 */
export const $singleAlbum = createStore(null).on(
  getAlbum.done,
  (_, { result }) => result
);

/**
 * Get Single Photo
 */
export const getSinglePhoto = createEffect("Get Single Photo").use(
  async id =>
    await new Promise(resolve =>
      window.FB.api(
        `/${id}`,
        {
          fields: "id, name, images, alt_text, name_tags, picture"
        },
        response => resolve(response)
      )
    )
);

/**
 * Single Album
 */
export const $singlePhoto = createStore(null).on(
  getSinglePhoto.done,
  (_, { result }) => result
);

/**
 * Upload Photo
 */
export const uploadPhoto = createEffect("Upload Photo").use(
  async data =>
    await new Promise(resolve =>
      window.FB.api(`/me/photos`, "POST", data, response => {
        console.log(response);
        return resolve(response);
      })
    )
);
