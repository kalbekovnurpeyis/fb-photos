import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { NavLink } from "react-router-dom";
import { formatRelative } from "date-fns";

import { ArrowDoubleRightIcon, ClockIcon, ImagesIcon } from "./icons";

const getAlbumPhoto = photo => {
  if (photo) {
    return photo.picture;
  }
  return "https://p2d7x8x2.stackpathcdn.com/wordpress/wp-content/themes/thedrinksbusiness/images/no_img_available.jpg";
};

export const Album = ({ album }) => {
  return (
    <AlbumWrapper to={`/albums/${album.id}`}>
      <AlbumImage
        bgImage={getAlbumPhoto(album.cover_photo)}
        title={album.name}
      />
      <AlbumInfo>
        <AlbumTitle>{album.name}</AlbumTitle>
        <MetaData>
          <ImagesIcon />
          <span className="ml-2">{album.count} files</span>
        </MetaData>
        <MetaData>
          <ClockIcon />
          <span className="ml-2">
            {formatRelative(new Date(album.created_time), new Date())}
          </span>
        </MetaData>
      </AlbumInfo>
      <ArrowDoubleRightIcon color="#3b60b8" size={32} />
    </AlbumWrapper>
  );
};

Album.propTypes = {
  album: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    created_time: PropTypes.string.isRequired,
    count: PropTypes.number.isRequired
  }).isRequired
};

const AlbumWrapper = styled(NavLink)`
  display: flex;
  align-items: center;
  border-top: 1px solid #eee;
  padding: 1rem 2rem;
  text-decoration: none !important;
  transition: 0.5s;

  &:last-child {
    border-bottom: 1px solid #eee;
  }

  &:hover {
    background-color: #eee;
    transition: 0.25s;
  }
`;

const AlbumImage = styled.div`
  width: 100px;
  height: 100px;
  overflow: hidden;
  background-image: url(${({ bgImage }) => bgImage});
  background-size: cover;
  background-position: center;

  img {
    max-width: 100%;
    max-height: 100%;
  }
`;

const AlbumInfo = styled.div`
  flex: 1;
  width: 100%;
  margin-left: 1rem;
`;

const AlbumTitle = styled.div`
  color: #3b60b8;
  font-size: 1.5rem;
  font-weight: bold;
  margin-bottom: 1rem;
`;

const MetaData = styled.div`
  color: #6c757d;
  font-size: 1rem;
  display: flex;
  align-items: center;
  margin-top: 5px;
`;
