import React from "react";

import { PageWrapper } from "../components/page-wrapper";
import { uploadPhoto } from "../models/fb-albums";

export const UploadPhotos = () => {
  const [formData, setFormData] = React.useState({
    source: null,
    caption: null
  });

  React.useEffect(() => {
    console.log(formData);
  }, [formData]);

  const handleChange = event => {
    setFormData({ ...formData, [event.target.name]: event.target.value });
  };

  const handleSubmit = e => {
    e.persist();
    e.preventDefault();

    uploadPhoto({ ...formData, published: false });
  };

  return (
    <PageWrapper>
      <h1>Upload Photos</h1>

      <form onSubmit={handleSubmit} encType="multipart/form-data">
        <input type="file" name="source" onChange={handleChange} />
        <input type="text" name="caption" onChange={handleChange} />
        <button type="submit">Send</button>
      </form>
    </PageWrapper>
  );
};
