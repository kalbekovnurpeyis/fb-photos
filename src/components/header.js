import React from "react";
import { Button, Container, Navbar, NavbarBrand } from "reactstrap";
import { useStore } from "effector-react";
import styled from "styled-components";
import { NavLink } from "react-router-dom";

import { logout, $logStatus, $userInfo } from "../models/fb-auth";

export const Header = () => {
  const userInfo = useStore($userInfo);
  const logStatus = useStore($logStatus);
  const logoutPending = useStore(logout.pending);

  return (
    <Navbar color="dark" dark className="mb-4">
      <Container>
        <NavbarBrand tag={NavLink} to="/">
          FACEBOOK PHOTOS
        </NavbarBrand>

        <div style={{ display: "flex" }}>
          {userInfo && (
            <UserInfo>
              <AvatarWrap>
                {userInfo.picture && (
                  <img
                    src={userInfo.picture.data.url}
                    alt={`${userInfo.first_name} ${userInfo.last_name}`}
                  />
                )}
              </AvatarWrap>
              <UserName>{`${userInfo.first_name} ${userInfo.last_name}`}</UserName>
            </UserInfo>
          )}

          {logStatus === "connected" && (
            <Button onClick={logout} disabled={logoutPending}>
              Logout
            </Button>
          )}
        </div>
      </Container>
    </Navbar>
  );
};

const UserInfo = styled.div`
  display: flex;
  align-items: center;
  margin-right: 30px;
`;

const AvatarWrap = styled.div`
  border: 1px solid #fff;
  border-radius: 50%;
  overflow: hidden;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 40px;
  height: 40px;
  background-color: #fff;

  img {
    max-width: 100%;
    max-height: 100%;
  }
`;

const UserName = styled.span`
  color: white;
  margin-left: 10px;
  text-transform: uppercase;
`;
