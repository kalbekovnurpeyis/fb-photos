import React from "react";
import { Link } from "react-router-dom";
import { Button } from "reactstrap";

export const NotFound = () => {
  return (
    <>
      <h1>Page Not Found</h1>
      <Button tag={Link} to="/" color="primary" className="mt-2">
        Go Home
      </Button>
    </>
  );
};
