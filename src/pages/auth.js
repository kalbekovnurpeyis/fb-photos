import React from "react";
import { Button } from "reactstrap";
import { useStore } from "effector-react";

import { login, $logStatus } from "../models/fb-auth";

export const Auth = ({ history }) => {
  const logStatus = useStore($logStatus);
  const loginPending = useStore(login.pending);

  React.useEffect(() => {
    if (logStatus === "connected") {
      history.replace("/");
    }
  });

  return (
    <>
      <h1>Auth Page</h1>
      <Button
        onClick={login}
        disabled={loginPending}
        color="primary"
        className="mt-3"
      >
        Login
      </Button>
    </>
  );
};
