import React from "react";
import styled from "styled-components";
import { useHistory } from "react-router-dom";

import { ArrowLeftIcon, FolderIcon } from "../components/icons";

export const AlbumHeader = ({ title, ...props }) => {
  const history = useHistory();

  return (
    <Wrapper>
      <GoBack onClick={history.goBack} className="p-1 mr-2">
        <ArrowLeftIcon color="#3b60b8" />
      </GoBack>

      <FolderIcon color="#3b60b8" />
      <span className="ml-1">{title}</span>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  border-bottom: 1px solid #eee;
  padding-bottom: 10px;
  margin-bottom: 10px;
`;

const GoBack = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 30px;
  height: 30px;
  border-radius: 50%;
  cursor: pointer;
  transition: 0.25s;

  &:hover {
    background-color: #eee;
  }
`;
